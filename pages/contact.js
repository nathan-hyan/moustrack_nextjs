import { useState } from 'react'
import Layout from '../components/Layout'
import { Form, Row, Col } from 'react-bootstrap'
import { background } from '../components/_info'
import MousButton from '../components/subcomponents/MousButton'
import emailjs from 'emailjs-com'



const contact = () => {
    const [validated, setValidated] = useState(false);
    const [from, setFrom] = useState("")
    const [birthday, setBirthday] = useState("")
    const [replyMail, setReplyMail] = useState("")
    const [isLoading, setIsLoading] = useState(false)

    function submit(event) {
        event.preventDefault();

        const form = event.currentTarget;

        if (form.checkValidity() === false) {
            event.stopPropagation();
        }
        if (form.checkValidity() === true) {
            event.stopPropagation();
            setIsLoading(true)
            emailjs.sendForm('gmail', process.env.NEXT_PUBLIC_TEMPLATE_ID, event.target, process.env.NEXT_PUBLIC_USER_ID)
                .then(() => { alert("Email sent! We will contact you as soon as posible!"); setIsLoading(false) },
                    () => { alert("There was an error sending the email. Please try again"); setIsLoading(false) })
        }

        setValidated(true);
    }

    return (
        <Layout>
            <div style={background.a} className="vh-custom mt-5 d-flex justify-content-center align-items-center ">
                <Row>
                    <Col>
                        <Row>
                            <Col />
                            <Col md={4} className='p-5 text-uppercase bg-warning text-dark'>
                                <h1 className='font-weight-bolder'>subscribe to <span className='text-black'>moustrack</span> so you can be the <span className='text-black'>first</span> to get all
                            the <span className="text-black">new information</span>
                                </h1>
                            </Col>
                            <Col md={5} className='p-5 bg-dark text-light'>
                                <Form noValidate validated={validated} onSubmit={submit}>
                                    <Form.Group>
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control name='from' value={from} onChange={(e) => setFrom(e.target.value)} type="text" required />
                                        <Form.Control.Feedback type="invalid">Please enter your name.</Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group>
                                        <Form.Label>E-mail address</Form.Label>
                                        <Form.Control name='replyMail' value={replyMail} onChange={(e) => setReplyMail(e.target.value)} type="email" required />
                                        <Form.Control.Feedback type="invalid">Please enter your e-mail.</Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group>
                                        <Form.Label>Birthdate</Form.Label>
                                        <Form.Control name='birthday' value={birthday} onChange={(e) => setBirthday(e.target.value)} type="date" required />
                                        <Form.Control.Feedback type="invalid">Please enter your birthday.</Form.Control.Feedback>
                                    </Form.Group>

                                    <MousButton isLoading={isLoading} isLoadingLabel='sending mail' className='' type="submit" label='Submit' />
                                </Form>
                            </Col>
                            <Col />
                        </Row>
                    </Col>
                </Row>
            </div>
        </Layout>
    )
}

export default contact