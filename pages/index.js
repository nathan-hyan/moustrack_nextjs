import Layout from '../components/Layout'
import Hero from '../components/Hero'
import Information from '../components/Information'
import Tracks from '../components/Tracks'
import About from '../components/About'

const Home = () => (
  <Layout>
    <Hero />
    <Information />
    <Tracks />
    <About />
  </Layout>
)

export default Home;