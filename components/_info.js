let _info = {}
let _modalData = {}

_info.tracks = {
    northImg: '/img/north-a.webp',
    southImg: '/img/south-a.webp',
    europImg: '/img/euro.webp'
}

_info.bg = {
    a: { background: 'url(/img/bghero.webp)', backgroundSize: 'cover', backgroundPosition: 'center' },
    b: { background: 'url(/img/bghero2.webp)', backgroundSize: 'cover', backgroundPosition: 'center' },
    c: { background: 'url(/img/bghero3.webp)', backgroundSize: 'cover', backgroundPosition: 'center' },
}

_modalData = {
    information: {
        title: "welcome to an unforgettable journey",
        body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium debitis delectus voluptatem iure rerum, libero provident minima officiis?\n\nEum id ex eius, non necessitatibus repudiandae quis, quas impedit nihil quidem, ad in maiores rem inventore adipisci cumque! Repudiandae, nesciunt expedita?",
        img: "/img/card-bg.webp"
    },
    north: {
        title: "North America trial",
        body: "Lorem ipsum dolor sit amet consectetur adipisicing elit.\n\nCorporis, aspernatur rerum? Tempore, cupiditate.\n\nDoloremque error a eius at reiciendis, quos quibusdam, sed accusantium ab consectetur ad, nulla aspernatur ratione molestiae.",
        img: "/img/north-a.webp"
    },
    south: {
        title: "South America trial",
        body: "Lorem ipsum dolor sit amet consectetur adipisicing elit.\n\nLaboriosam et beatae reiciendis libero vitae illo corrupti recusandae quaerat quidem.\n\nEum, maxime fugit? Cumque fugit possimus accusantium totam commodi eius illum deserunt!",
        img: "/img/south-a.webp"
    },
    europe: {
        title: "Europe trial",
        body: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.\n\nAutem aperiam nam laudantium architecto sed quo porro veritatis labore! Odio maxime id nostrum, nam autem earum. \n\nReprehenderit distinctio fugit magnam?",
        img: "/img/euro.webp"
    },
}

export const tracksImg = _info.tracks
export const background = _info.bg
export const modalData = _modalData