import Head from 'next/head'
import { Container, Navbar, Nav, Image } from 'react-bootstrap'

const Layout = (props) => {
    return (
        <>
            <Head>
                <title>moustrack | new roads awaits you.</title>
                <meta name="Description" content="Welcome to your new favorite travel agancy. From mountains to cities, moustrack tours are full of wonderful natural and artificial settings.." />
                <link rel="icon" href="/favicon.ico" />
                <script src="https://smtpjs.com/v3/smtp.js" />
            </Head>
            <Navbar bg="warning" variant='light' expand="lg" style={{ zIndex: 20 }} className=' shadow' fixed='top'>
                <Navbar.Brand href="/#home"><Image src="/img/logo-black.webp" id='moustrackLogo' alt='moustrack Logo' /></Navbar.Brand>
                <Navbar.Toggle aria-controls="nav" />
                <Navbar.Collapse id="nav">
                    <Nav className="mr-auto font-weight-bold text-uppercase">
                        <Nav.Link href='/#home'>Home</Nav.Link>
                        <Nav.Link href='/#roads'>New roads</Nav.Link>
                        <Nav.Link href='/#tracks'>Tracks</Nav.Link>
                        <Nav.Link href='/#about'>About</Nav.Link>
                    </Nav>
                    <Nav className='ml-auto font-weight-bold text-uppercase'>
                        <Nav.Link href='https://hyan.dev'>Volver al portfolio</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <Container fluid>
                {props.children}
            </Container>
        </>
    )
}
export default Layout
