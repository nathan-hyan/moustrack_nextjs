import { Row, Col, Image } from "react-bootstrap"
import Link from 'next/link'
import MousButton from "./subcomponents/MousButton"
import { background } from './_info'

const Hero = () => {
    return (
        <section id='home' style={background.a} className='vh-100 d-flex justify-content-center align-items-center px-5'>
            <Row className='w-100'>
                <Col sm={12} md={6} className='text-center text-md-left my-auto zIndex'>
                    <h1 className='py-3 text-uppercase font-weight-bold text-warning'>Your journey begins here</h1>
                    <Link href='/contact'><a><MousButton label='Start Now' /></a></Link>
                </Col>
                <Col sm={12} md={6} className='text-center text-md-right zIndex'>
                    <Image src='/img/world.webp' height='150' width='150' className='d-none d-md-inline' />
                    <p className='lead text-light d-block d-sm-none mt-3'>millions of competitors around the world are already warming-up for the biggest event of <strong>their lives.</strong></p>
                    <p className='lead text-dark d-none d-md-block mt-3'>millions of competitors around the world are already warming-up for the biggest event of <strong>their lives.</strong></p>
                </Col>
            </Row>
            <div className='skew-container d-none d-md-block'>
                <div className="skew-background" />
            </div>
        </section>
    )
}

export default Hero