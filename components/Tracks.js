import TrackCard from "./subcomponents/TrackCard"
const { Row, Col } = require("react-bootstrap")
import { tracksImg, background, modalData } from './_info'
import CustomModal from "./subcomponents/CustomModal"
import { useState } from 'react'

const Tracks = () => {
    const [showNA, setShowNA] = useState(false)
    const [showSA, setShowSA] = useState(false)
    const [showEUP, setShowEUP] = useState(false)
    const handleCloseNA = () => setShowNA(!showNA)
    const handleCloseSA = () => setShowSA(!showSA)
    const handleCloseEUP = () => setShowEUP(!showEUP)

    return (
        <section id='tracks' style={background.c} className='d-flex align-items-center'>
            <CustomModal show={showNA} handleClose={handleCloseNA} data={modalData.north} />
            <CustomModal show={showSA} handleClose={handleCloseSA} data={modalData.south} />
            <CustomModal show={showEUP} handleClose={handleCloseEUP} data={modalData.europe} />

            <div className='w-100'>
                <Row className='my-5'>
                    <Col sm={0} md={4} />
                    <Col sm={12} md={4} className='text-center'>
                        <h1 className='bg-warning text-dark py-3 px-5 d-block text-uppercase font-weight-bold'>Choose your destination</h1>

                    </Col>
                    <Col sm={0} md={4} />
                </Row>
                <div className="px-md-5 mb-5">
                    <Row>
                        <Col sm={12}>
                            <TrackCard className='d-none d-md-block' title='north america' leftSide img={tracksImg.northImg} />
                            <TrackCard className='d-block d-md-none' title='north america' leftSide img={tracksImg.northImg} onClick={handleCloseNA} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} className='my-5'>
                            <TrackCard className='d-none d-md-block' title='south america' img={tracksImg.southImg} />
                            <TrackCard className='d-block d-md-none' title='south america' img={tracksImg.southImg} onClick={handleCloseSA} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <TrackCard className='d-none d-md-block' title='europe' leftSide img={tracksImg.europImg} />
                            <TrackCard className='d-block d-md-none' title='europe' leftSide img={tracksImg.europImg} onClick={handleCloseEUP} />
                        </Col>
                    </Row>
                </div>
            </div>
        </section>
    )
}

export default Tracks