import { useState } from 'react'
import { Row, Col } from 'react-bootstrap'
import { background } from './_info'
import MousButton from './subcomponents/MousButton'
import CustomModal from './subcomponents/CustomModal'
import { modalData } from './_info'

const Information = () => {
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(!show)

    return (
        <section id='roads' style={background.b} className='text-center vh-100 zIndex'>

            <CustomModal show={show} handleClose={handleClose} data={modalData.information} />
            <Row className='h-100'>
                <Col />
                <Col sm={12} md='auto' className=' my-auto bg-warning text-black p-5 shadow'>
                    <h1 className='font-weight-bold text-uppercase'>New roads await you</h1>
                    <p className='lead mb-5'>From mountains to cities, moustrack tours are full of wonderful natural and artificial settings.</p>
                    <MousButton invert label='Learn More' onClick={handleClose} />
                </Col>
                <Col />
            </Row>
        </section >
    )
}

export default Information