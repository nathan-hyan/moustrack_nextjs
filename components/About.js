import { Row, Col, Image } from 'react-bootstrap'
import { background } from "./_info"

const About = () => (
    <section id='about' style={background.a} className='vh-100'>
        <Row className='py-5'>
            <Col sm={0} md={6} />
            <Col className='text-center text-md-right'>
                <div className="py-3 px-3 px-md-5 bg-warning text-dark text-uppercase">
                    <h1 className='font-weight-bolder d-block d-md-none'><Image src='/img/logo-black.webp' alt='moustrack Logo' /></h1>
                    <h1 className='font-weight-bolder d-none d-md-inline display-4'><Image src='/img/logo-black.webp' alt='moustrack Logo' /></h1>
                    <p className='font-weight-bold'>Your Go-to travel manager</p>
                </div>
                <div className="mt-5 px-3 px-md-5 text-light lead">
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eveniet cum perspiciatis accusantium ratione molestias corrupti. Quo reiciendis quod, totam illum harum omnis culpa expedita, tempora minima deserunt earum eum molestiae, voluptates laboriosam consequatur excepturi provident?</p>
                </div>
                <div className="text-muted px-md-5 px-3">
                    <small>designed by Hy-An Multimedia Group</small>
                    <br />
                    <small>images by unsplash.com</small>
                </div>
            </Col>
        </Row>
    </section>
)

export default About