import MousButton from './MousButton'
import { Card } from 'react-bootstrap'

const TrackCard = (props) => (
    <Card className={`${props.className} bg-dark text-white overflow-hidden shadow border border-thick border-warning`} onClick={props.onClick}>
        <div className='fixed-card-size' style={{ background: `url(${props.img})` }} />
        <Card.ImgOverlay className={`h-100 d-flex overlay-img ${!props.leftSide ? 'text-right' : 'text-left'}`}>
            <div className="my-auto w-100 ">
                <Card.Title className='display-5 text-uppercase font-weight-bold text-shadow p-0 m-0'>{props.title}</Card.Title>
                <Card.Text className='mt-3 d-none d-md-block'><MousButton label='More Info' onClick={props.onClick} /></Card.Text>
            </div>
        </Card.ImgOverlay>
    </Card>
)


export default TrackCard