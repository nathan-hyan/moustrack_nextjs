import { Button, Spinner } from "react-bootstrap"

const MousButton = (props) => (
    <Button disabled={props.isLoading} type={props.type} onClick={props.onClick} variant={props.invert ? 'dark' : 'warning'} className={`${props.invert ? 'text-warning' : 'text-dark'} py-2 px-4 text-uppercase font-weight-bold ${props.className}`}><Spinner animation="border" role="status" size='sm' className={props.isLoading ? 'mr-2' : 'd-none'} />{props.isLoading ? props.isLoadingLabel : props.label}</Button>
)

export default MousButton