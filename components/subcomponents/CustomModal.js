import { Modal, Col, Row } from 'react-bootstrap'
import MousButton from './MousButton'
import Link from 'next/link'


const CustomModal = (props) => {
    const style = {
        header: {
            background: '#3f3f3f',
            color: '#F2C010',
            textTransform: 'uppercase',
            fontWeight: 'bold',
            border: 0
        },

        body: {
            background: '#333',
            color: '#fafafa',
            border: '#292929 solid 3px',
            borderRadius: 5
        },

        cardBackground: {
            background: `url("${props.data.img}")`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            height: 500
        },

        buttons: {
            bottom: 0,
            right: 0,
            position: 'absolute'
        },

        parents: {
            position: 'relative'
        }
    }


    return (
        <Modal show={props.show} onHide={props.handleClose} size='lg'>
            <Modal.Body className='row shadow' style={style.body}>
                <Col style={style.cardBackground} md={4} />
                <Col style={style.parents}>
                    <h1 className='display-5 text-uppercase text-warning font-weight-bolder'>{props.data.title}</h1>
                    <p className='lead custom-white-space'>{props.data.body}</p>
                    <div className='text-right mt-auto' style={style.buttons}>
                        <MousButton invert label='Close' onClick={props.handleClose} className='mr-3 ' />
                        <Link href='/contact'><a><MousButton label='Contact' /></a></Link>
                    </div>
                </Col>
            </Modal.Body>
        </Modal>
    )
}


export default CustomModal